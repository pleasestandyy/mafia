package com.example.mafia

class Const {
    companion object {
        //game phase
        const val NEW_GAME_PHASE = -1
        const val ZERO_ROUND_PHASE = 0

        //colors
        const val GRAY_COLOR_DEFAULT = -10395295
        
        // bundle keys
        const val GAME_ID = "gameId"
        const val GAME_NAME = "gameName"
    }
}