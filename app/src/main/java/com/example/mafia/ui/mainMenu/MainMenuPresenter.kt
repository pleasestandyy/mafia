package com.example.mafia.ui.mainMenu

import android.annotation.SuppressLint
import android.app.Application
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.mafia.MafiaApp
import com.example.mafia.data.MafiaDatabase
import com.example.mafia.data.dao.GameDao
import com.example.mafia.data.model.Game
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.withContext
import java.util.*
import java.text.SimpleDateFormat


@InjectViewState
class MainMenuPresenter: MvpPresenter<MainMenuView>() {


    @Inject
    lateinit var db: MafiaDatabase

    @Inject
    lateinit var context: Application

    var gameDao: GameDao

    init {
        MafiaApp.getAppComponent().inject(this)
        gameDao = db.gameDao()
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

    @SuppressLint("SimpleDateFormat")
    fun startNewGame(name: String){
        CoroutineScope(IO).launch {
            val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm")
            val time = dateFormat.format(Calendar.getInstance().time)

            val game = Game(name = name, date = time, gamePhase = -1)
            val gameId = gameDao.insert(game)
            withContext(Main){
                viewState.openNewGameFragment(gameId, name)
            }
        }
    }

}