package com.example.mafia.ui.continueGame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.mafia.Const
import com.example.mafia.R
import com.example.mafia.data.model.Game
import com.example.mafia.listAdapter.ItemTouchHelperCallback
import com.example.mafia.listAdapter.ListAdapter
import com.example.mafia.listAdapter.holder.GameHolder
import com.example.mafia.listAdapter.interfaces.DragAndSwipeItemsListner
import com.example.mafia.listAdapter.interfaces.OnItemClickListner
import kotlinx.android.synthetic.main.continue_game_fragment.*

import com.example.mafia.ui.interfaces.InizializbleViews
import com.example.mafia.ui.newGame.NewGameFragment
import com.example.mafia.ui.zeroRound.ZeroRoundFragment

import java.util.concurrent.CopyOnWriteArrayList

class ContinueGameFragment: MvpAppCompatFragment(), ContinueGameView, InizializbleViews {

    companion object{
        fun getInstance(): ContinueGameFragment{
            return ContinueGameFragment()
        }
    }

    @InjectPresenter
    lateinit var presenter: ContinueGamePresenter

    @ProvidePresenter
    fun providePresenter(): ContinueGamePresenter{
        return ContinueGamePresenter()
    }

    private lateinit var gameListAdapter: ListAdapter<Game>
    private lateinit var touchHelperGame : ItemTouchHelper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View? = inflater.inflate(R.layout.continue_game_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        inizializeViews()
    }

    override fun inizializeViews() {
        initList()
    }

    fun initList(){
        gameListAdapter = ListAdapter(R.layout.game_item, GameHolder::class)
        gameListAdapter.onItemClickListner = object : OnItemClickListner {
            override fun onitemClick(position: Int) {
                val game = gameListAdapter.getItem(position)
                var fragment : MvpAppCompatFragment? = null
                when(game.gamePhase){
                    Const.NEW_GAME_PHASE ->
                        fragment = NewGameFragment.getInstance(game.id!!, game.name!!)
                    Const.ZERO_ROUND_PHASE ->
                        fragment = ZeroRoundFragment.getInstance(game.id!!)
                }
                openFragment(fragment!!)
            }
        }
        recyclerViewGame.layoutManager = LinearLayoutManager(context)
        recyclerViewGame.adapter = gameListAdapter

        gameListAdapter.onDragAndSwipeItemsListner = object : DragAndSwipeItemsListner{
            override fun onSwapItems(pos1: Int, pos2: Int) {
            }

            override fun onSwipeItem(pos: Int) {
                presenter.removeGameItem(pos)
            }

        }

        val callback = ItemTouchHelperCallback(gameListAdapter)
        callback.isDragBlocked = true
        touchHelperGame = ItemTouchHelper(callback)
        touchHelperGame.attachToRecyclerView(recyclerViewGame)
    }

    fun openFragment(fragment: MvpAppCompatFragment){
        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }
    override fun showGameList(gameList: CopyOnWriteArrayList<Game>) {
        gameListAdapter.setList(gameList)
        gameListAdapter.notifyDataSetChanged()
    }
    override fun showMassege(massage: String) {
        textViewMassage.visibility = View.VISIBLE
        textViewMassage.setText(massage)
    }

    override fun hideMassage() {
        textViewMassage.visibility = View.GONE
    }
}
