package com.example.mafia.ui.mainMenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.mafia.Const
import com.example.mafia.R
import com.example.mafia.ui.continueGame.ContinueGameFragment
import com.example.mafia.ui.newGame.NewGameFragment
import com.example.mafia.ui.interfaces.InizializbleViews
import kotlinx.android.synthetic.main.main_menu_fragment.*

class MainMenuFragment: MvpAppCompatFragment(), InizializbleViews ,
    MainMenuView {

    companion object{
        fun getInstance(): MainMenuFragment{
            return MainMenuFragment()
        }
    }
    @InjectPresenter
    lateinit var presenter: MainMenuPresenter

    @ProvidePresenter
    fun providePresenter(): MainMenuPresenter {
        return MainMenuPresenter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View? = inflater.inflate(R.layout.main_menu_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inizializeViews()
    }

    override fun inizializeViews(){
        val suportFragmentManager = activity!!.supportFragmentManager
        newGameButton.setOnClickListener{

            MaterialDialog(activity!!).show {
                input(waitForPositiveButton = false, hint = "Введите название игры..."){ dialog, text ->
                }
                val input = getInputField()
                positiveButton(R.string.ok){
                    presenter.startNewGame(input.text.toString())
                }
            }

        }

        continueGameButton.setOnClickListener{
                v -> suportFragmentManager
            .beginTransaction()
            .replace(R.id.container, ContinueGameFragment.getInstance())
            .addToBackStack(null)
            .commit()
        }
    }


    override fun openNewGameFragment(gameId: Long, gameName: String) {
        val fragment = NewGameFragment.getInstance(gameId,gameName)
        activity!!.supportFragmentManager
        .beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }

}
