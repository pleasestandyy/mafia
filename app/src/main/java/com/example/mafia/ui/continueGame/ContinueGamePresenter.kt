package com.example.mafia.ui.continueGame

import android.app.Application
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.mafia.MafiaApp
import com.example.mafia.R
import com.example.mafia.data.MafiaDatabase
import com.example.mafia.data.dao.GameDao
import com.example.mafia.data.model.Game
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.CopyOnWriteArrayList
import javax.inject.Inject

@InjectViewState
class ContinueGamePresenter: MvpPresenter<ContinueGameView>() {

    private var gameList: CopyOnWriteArrayList<Game>
    private var gameDao: GameDao

    @Inject
    lateinit var db : MafiaDatabase

    @Inject
    lateinit var context: Application
    init {
        MafiaApp.getAppComponent().inject(this)
        gameDao = db.gameDao()
        gameList = CopyOnWriteArrayList()
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        setGameList()
    }

    fun setGameList(){
        CoroutineScope(Dispatchers.IO).launch {
            gameList.addAll(gameDao.getAll())
            withContext(Dispatchers.Main){
                if(gameList.isEmpty()) {
                    viewState.showMassege(context.getString(R.string.list_is_empty))
                } else{
                    viewState.hideMassage()
                    viewState.showGameList(gameList)
                }
            }
        }
    }

    fun removeGameItem(pos: Int){
        CoroutineScope(Dispatchers.IO).launch {
            gameDao.delete(gameList[pos])
            gameList.removeAt(pos)
        }
    }

}