package com.example.mafia.ui.newGame

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.mafia.data.model.Player
import com.example.mafia.data.model.Role
import java.util.concurrent.CopyOnWriteArrayList

interface NewGameView : MvpView {

    fun showRoleList(roleList: CopyOnWriteArrayList<Role>)

    fun showPlayerList(playerList: CopyOnWriteArrayList<Player>)

    fun setGameName(name: String)

    @StateStrategyType(SkipStrategy::class)
    fun openZeroFragment(gameId: Long)

    @StateStrategyType(SkipStrategy::class)
    fun showMassege(massage: String)

    @StateStrategyType(SkipStrategy::class)
    fun hideMassage()

    @StateStrategyType(SkipStrategy::class)
    fun notifyRoleListChanged()

    @StateStrategyType(SkipStrategy::class)
    fun notifyPlayerListChanged()

}