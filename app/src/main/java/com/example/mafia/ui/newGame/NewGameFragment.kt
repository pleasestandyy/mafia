package com.example.mafia.ui.newGame

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.color.ColorPalette
import com.afollestad.materialdialogs.color.colorChooser
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.mafia.Const
import com.example.mafia.R
import com.example.mafia.listAdapter.ListAdapter
import com.example.mafia.listAdapter.ItemTouchHelperCallback
import com.example.mafia.data.model.Player
import com.example.mafia.data.model.Role
import com.example.mafia.listAdapter.holder.PlayerHolder
import com.example.mafia.listAdapter.holder.RoleHolder
import com.example.mafia.listAdapter.interfaces.DragAndSwipeItemsListner
import com.example.mafia.ui.interfaces.InizializbleViews
import com.example.mafia.ui.zeroRound.ZeroRoundFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.add_player_dialog.*
import kotlinx.android.synthetic.main.add_role_dialog.*
import kotlinx.android.synthetic.main.new_game_fragment.*
import kotlinx.android.synthetic.main.new_game_fragment.textViewMassage
import java.util.concurrent.CopyOnWriteArrayList



class NewGameFragment : MvpAppCompatFragment(), NewGameView, InizializbleViews{

    companion object {
        fun getInstance(id: Long, name: String): NewGameFragment{
            val fragment  = NewGameFragment()

            val bundle = Bundle()
            bundle.putLong(Const.GAME_ID, id)
            bundle.putString(Const.GAME_NAME, name)

            fragment.arguments = bundle
            return fragment
        }
    }
    @InjectPresenter
    lateinit var presenter: NewGamePresenter

    @ProvidePresenter
    fun providePresenter(): NewGamePresenter {
        return NewGamePresenter(arguments!!.getLong(Const.GAME_ID))
    }

    private lateinit var touchHelperRole : ItemTouchHelper

    private lateinit var touchHelperPlayer : ItemTouchHelper

    private lateinit var roleListAdapter: ListAdapter<Role>

    private lateinit var playerListAdapter: ListAdapter<Player>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View? = inflater.inflate(R.layout.new_game_fragment, container, false)
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextName.setText(arguments!!.getString(Const.GAME_NAME))
        inizializeViews()
    }

    override fun inizializeViews() {
        buttonAddPlayer.setOnClickListener {
            MaterialDialog(activity!!, BottomSheet(LayoutMode.WRAP_CONTENT)).show {
                title(R.string.add_player)
                customView(R.layout.add_player_dialog,scrollable = true,horizontalPadding = true)
                positiveButton(R.string.add) {
                    presenter.addPlayer(Player(name = editTextPlayerName.text.toString(),priority = playerListAdapter.size()))
                }
                negativeButton(R.string.cansel)
                lifecycleOwner(this@NewGameFragment)
            }
        }

         buttonAddRole.setOnClickListener {
             val dialog = MaterialDialog(activity!!, BottomSheet(LayoutMode.WRAP_CONTENT)).show {
                 title(R.string.add_role)
                 customView(R.layout.add_role_dialog,scrollable = true,horizontalPadding = true)
                 positiveButton(R.string.add){
                     for (i in 1..sliderRole.value.toInt()){
                         val role = Role(
                             title = editTextRoleName.text.toString(),
                             isWakeUpToNight = switchIsWakeUp.isChecked,
                             priority = roleListAdapter.size()
                         )
                         role.color = buttonColor.backgroundTintList!!.defaultColor
                         role.descripotion = editTextRoleDescription.text.toString()
                         presenter.addRole(role)
                     }
                 }
                 negativeButton(R.string.cansel)
                 lifecycleOwner(this@NewGameFragment)
             }
             val button = dialog.findViewById<MaterialButton>(R.id.buttonColor)
             button.backgroundTintList = ColorStateList.valueOf(Const.GRAY_COLOR_DEFAULT)
             button.setOnClickListener {
                 MaterialDialog(activity!!, BottomSheet(LayoutMode.WRAP_CONTENT)).show {
                     title(R.string.color)
                     colorChooser(
                         colors = ColorPalette.Primary,
                         subColors = ColorPalette.PrimarySub,
                         allowCustomArgb = true,
                         showAlphaSelector = true
                     ) { _, color ->
                          button.backgroundTintList = ColorStateList.valueOf(color)
                     }

                     positiveButton(R.string.add)
                     negativeButton(android.R.string.cancel)
                     lifecycleOwner(this@NewGameFragment)
                 }
             }
             val chipRoleNameGroup = dialog.findViewById<ChipGroup>(R.id.chipGroup)
             val editTextRoleName = dialog.findViewById<EditText>(R.id.editTextRoleName)

             val roleNamesDefault = resources.getStringArray(R.array.role_names_default)
             for(name in roleNamesDefault){
                 val chip = Chip(chipRoleNameGroup.context)
                 chip.text = name
                 chip.setOnClickListener {
                     editTextRoleName.setText(name, TextView.BufferType.EDITABLE)
                 }
                 chipRoleNameGroup.addView(chip)
             }
         }


         fab.setOnClickListener {
             if(roleListAdapter.size() >= presenter.MIN_SIZE_LISTS || playerListAdapter.size() >= presenter.MIN_SIZE_LISTS) {
                 if (!roleListAdapter.size().equals(playerListAdapter.size())) {
                     Snackbar.make(
                         view!!,
                         getString(R.string.role_and_player_is_equals),
                         Snackbar.LENGTH_SHORT
                     ).show()
                 } else {
                     presenter.openZeroRound(editTextName.text.toString())

                 }
             } else {
                 Snackbar.make(
                     view!!,
                     getString(R.string.role_and_player_min_size),
                     Snackbar.LENGTH_SHORT
                 ).show()
             }
         }
         initLists()
     }
    private fun initLists(){
        roleListAdapter = ListAdapter(R.layout.role_item, RoleHolder::class)
        roleListAdapter.setList(CopyOnWriteArrayList<Role>())
        recyclerViewRole.layoutManager = LinearLayoutManager(context)
        recyclerViewRole.adapter = roleListAdapter
        roleListAdapter.onDragAndSwipeItemsListner = object : DragAndSwipeItemsListner{
            override fun onSwapItems(pos1: Int, pos2: Int) {
                presenter.swapRoleItems(pos1, pos2)
            }

            override fun onSwipeItem(pos: Int) {
                presenter.removeRoleItem(pos)
                fab.show()
            }

        }

        touchHelperRole = ItemTouchHelper(ItemTouchHelperCallback(roleListAdapter))
        touchHelperRole.attachToRecyclerView(recyclerViewRole)

        recyclerViewRole.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (dy < 0) {
                    fab.show()

                } else if (dy > 0) {
                    fab.hide()
                }
            }
        })

        playerListAdapter = ListAdapter(R.layout.player_item, PlayerHolder::class)
        playerListAdapter.setList(CopyOnWriteArrayList<Player>())

        recyclerViewPlayer.layoutManager = LinearLayoutManager(context)
        recyclerViewPlayer.adapter = playerListAdapter
        playerListAdapter.onDragAndSwipeItemsListner = object : DragAndSwipeItemsListner{
            override fun onSwapItems(pos1: Int, pos2: Int) {
                presenter.swapPlayerItems(pos1, pos2)
            }

            override fun onSwipeItem(pos: Int) {
                presenter.removePlayerItem(pos)
                fab.show()
            }
        }

        touchHelperPlayer = ItemTouchHelper(ItemTouchHelperCallback(playerListAdapter))
        touchHelperPlayer.attachToRecyclerView(recyclerViewPlayer)

        recyclerViewPlayer.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0) {
                    fab.show()

                } else if (dy > 0) {
                    fab.hide()
                }
            }
        })
    }

    override fun showRoleList(roleList: CopyOnWriteArrayList<Role>) {
        roleListAdapter.setList(roleList)
        roleListAdapter.notifyDataSetChanged()
    }

    override fun showPlayerList(playerList: CopyOnWriteArrayList<Player>) {
        playerListAdapter.setList(playerList)
        playerListAdapter.notifyDataSetChanged()
    }

    override fun setGameName(name: String) {
        editTextName.setText(name)
    }

    override fun openZeroFragment(gameId: Long) {
        val fragment = ZeroRoundFragment()
        val bundle = Bundle()
        bundle.putLong(Const.GAME_ID, presenter.gameId)
        fragment.arguments = bundle
        activity!!.supportFragmentManager.popBackStack()
        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun showMassege(massage: String) {
        textViewMassage.visibility = View.VISIBLE
        textViewMassage.setText(massage)
    }

    override fun hideMassage() {
        textViewMassage.visibility = View.GONE
    }

    override fun notifyRoleListChanged() {
        recyclerViewRole.recycledViewPool.clear()
        roleListAdapter.notifyDataSetChanged()
    }

    override fun notifyPlayerListChanged() {
        recyclerViewPlayer.recycledViewPool.clear()
        playerListAdapter.notifyDataSetChanged()
    }



}
