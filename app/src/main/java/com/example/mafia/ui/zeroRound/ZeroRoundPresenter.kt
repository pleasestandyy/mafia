package com.example.mafia.ui.zeroRound

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.mafia.MafiaApp
import com.example.mafia.data.MafiaDatabase
import com.example.mafia.data.dao.PlayerDao
import com.example.mafia.data.dao.RoleDao
import com.example.mafia.data.model.Player
import com.example.mafia.data.model.Role
import com.example.mafia.data.model.RoleAndPlayer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.CopyOnWriteArrayList
import javax.inject.Inject

@InjectViewState
class ZeroRoundPresenter(private var gameId: Long): MvpPresenter<ZeroRoundView>() {

    @Inject
    lateinit var db: MafiaDatabase

    private var playerDao: PlayerDao
    private var roleDao: RoleDao

    private var roleList = CopyOnWriteArrayList<Role>()
    private var playerList = CopyOnWriteArrayList<Player>()
    private var roleAndPlayerList = CopyOnWriteArrayList<RoleAndPlayer>()

    init {
        MafiaApp.getAppComponent().inject(this)
        playerDao = db.playerDao()
        roleDao= db.roleDao()
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        CoroutineScope(IO).launch {
            val id = gameId
            val list = roleDao.getAllByIdGame(gameId)
            val list1 = playerDao.getAllByIdGame(gameId)
            roleList.addAll(roleDao.getAllByIdGame(gameId))
            playerList.addAll(playerDao.getAllByIdGame(gameId))
            val iterPlayerList = playerList.iterator()

            for(role in roleList){
                roleAndPlayerList.add(RoleAndPlayer(role))
            }

            while (iterPlayerList.hasNext()){
                val player = iterPlayerList.next()
                if(player.roleId != null){
                    for(item in roleAndPlayerList){
                        if(player.roleId == item.role!!.id){
                            item.player = player
                            playerList.remove(player)
                            roleList.remove(item.role)
                            break
                        }
                    }
                }
            }

            withContext(Main){
                if(playerList.isEmpty()){
                    viewState.showFab()
                }
                viewState.showList(roleAndPlayerList)
            }
        }
    }

    fun openRoleItem(pos: Int){
        val list = ArrayList<String>()
        for(player in playerList){
            list.add(player.name!!)
        }
        if(roleAndPlayerList[pos].player != null){
            viewState.openUnBindRoleDialog(pos)
        } else {
            viewState.openBindRoleDialog(roleAndPlayerList[pos].role!!, list)
        }
    }

    fun unbind(pos: Int){
        CoroutineScope(IO).launch {
            val player = roleAndPlayerList[pos].player
            player!!.roleId = null
            playerList.add(player)
            playerDao.update(player)
            roleAndPlayerList[pos].player = null
            withContext(Main){
                viewState.hideFab()
                viewState.notifyPlayerListChanged()
            }
        }

    }

    fun bindRoleToPlayer(playerIndex: Int, role: Role){
        CoroutineScope(IO).launch {
            playerList[playerIndex].roleId = role.id
            playerDao.update(playerList[playerIndex])
            for(item in roleAndPlayerList){
                if(item.role!! == role){
                    item.player = playerList[playerIndex]
                }
            }
            playerList.remove(playerList[playerIndex])
            roleList.remove(role)
            withContext(Main){
                if(playerList.isEmpty()){
                    viewState.showFab()
                }
                viewState.notifyPlayerListChanged()
            }
        }
    }
}