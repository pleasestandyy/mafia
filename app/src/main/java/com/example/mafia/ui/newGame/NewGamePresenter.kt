package com.example.mafia.ui.newGame

import android.app.Application
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.mafia.Const
import com.example.mafia.MafiaApp
import com.example.mafia.R
import com.example.mafia.data.MafiaDatabase
import com.example.mafia.data.dao.GameDao
import com.example.mafia.data.dao.PlayerDao
import com.example.mafia.data.dao.RoleDao
import com.example.mafia.data.model.Game
import com.example.mafia.data.model.Player
import com.example.mafia.data.model.Role
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.CopyOnWriteArrayList
import javax.inject.Inject

@InjectViewState
public class NewGamePresenter(val gameId: Long): MvpPresenter<NewGameView>() {

    val MIN_SIZE_LISTS = 3

    lateinit var game: Game

    private var roleList: CopyOnWriteArrayList<Role>
    private var playerList: CopyOnWriteArrayList<Player>

    private var roleDao: RoleDao
    private var playerDao: PlayerDao
    private var gameDao: GameDao

    @Inject
    lateinit var context : Application

    @Inject
    lateinit var db : MafiaDatabase

    init {
        MafiaApp.getAppComponent().inject(this)
        roleDao = db.roleDao()
        playerDao = db.playerDao()
        gameDao = db.gameDao()
        roleList = CopyOnWriteArrayList()
        playerList = CopyOnWriteArrayList()
    }
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        CoroutineScope(IO).launch {

            val tempRoleList : MutableList<Role> = roleDao.getAllByIdGame(gameId) as MutableList<Role>
            val tempPlayerList : MutableList<Player> = playerDao.getAllByIdGame(gameId) as MutableList<Player>
            game = gameDao.getGameById(gameId)

            tempPlayerList.sort()
            tempRoleList.sort()

            roleList.addAll(tempRoleList)
            playerList.addAll(tempPlayerList)


            withContext(Main){
                if(playerList.isEmpty() && roleList.isEmpty()) {
                    viewState.showMassege(context.getString(R.string.add_role_and_player))
                } else{
                    viewState.hideMassage()
                }
                viewState.showPlayerList(playerList)
                viewState.showRoleList(roleList)
            }
        }
    }

    fun swapRoleItems(pos1 : Int, pos2 : Int){
        roleList[pos1].priority = pos1
        roleList[pos2].priority = pos2
        CoroutineScope(IO).launch {
            roleDao.update(roleList[pos1])
            roleDao.update(roleList[pos2])
        }

    }


    fun swapPlayerItems(pos1 : Int, pos2 : Int){
        playerList[pos1].priority = pos1
        playerList[pos2].priority = pos2
        CoroutineScope(IO).launch {
            playerDao.update(playerList[pos1])
            playerDao.update(playerList[pos2])
        }

    }

    fun removeRoleItem(pos: Int){
        CoroutineScope(IO).launch {
            roleDao.delete(roleList[pos])
            roleList.removeAt(pos)
            if(pos < roleList.size){
                for(i in pos until roleList.size){
                    roleList[i].priority = roleList[i].priority!! - 1
                    roleDao.update(roleList[i])
                }
            }
            withContext(Main) {
                if (playerList.isEmpty() && roleList.isEmpty()) {
                    viewState.showMassege(context.getString(R.string.add_role_and_player))
                }
            }
        }
    }

    fun removePlayerItem(pos: Int){
        CoroutineScope(IO).launch {
            playerDao.delete(playerList[pos])
            playerList.removeAt(pos)
            if(pos < playerList.size){
                for(i in pos until playerList.size){
                    playerList[i].priority = playerList[i].priority!! - 1
                    playerDao.update(playerList[i])
                }
            }
            withContext(Main) {
                if (playerList.isEmpty() && roleList.isEmpty()) {
                    viewState.showMassege(context.getString(R.string.add_role_and_player))
                }
            }
        }
    }

    fun addPlayer(player: Player){
        player.gameId = gameId
        playerList.add(player)
        viewState.notifyPlayerListChanged()
        viewState.hideMassage()
        CoroutineScope(IO).launch {
            player.id = playerDao.insert(player)
        }
    }

    fun addRole(role: Role) {
        role.gameId = gameId
        roleList.add(role)
        viewState.notifyRoleListChanged()
        viewState.hideMassage()
        CoroutineScope(IO).launch {
            role.id = roleDao.insert(role)
        }
    }

    fun openZeroRound(gameName: String){
        game.gamePhase = Const.ZERO_ROUND_PHASE
        game.name = gameName
        CoroutineScope(IO).launch {
            gameDao.update(game)
            withContext(Main){
                viewState.openZeroFragment(gameId)
            }
        }
    }

}
