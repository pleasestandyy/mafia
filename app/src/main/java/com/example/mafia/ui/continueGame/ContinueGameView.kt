package com.example.mafia.ui.continueGame

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.mafia.data.model.Game
import java.util.concurrent.CopyOnWriteArrayList

@StateStrategyType(AddToEndSingleStrategy::class)
interface ContinueGameView : MvpView {

    fun showGameList(gameList: CopyOnWriteArrayList<Game>)

    fun showMassege(massage: String)

    fun hideMassage()
}