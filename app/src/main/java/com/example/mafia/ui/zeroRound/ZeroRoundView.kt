package com.example.mafia.ui.zeroRound

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.mafia.data.model.Role
import com.example.mafia.data.model.RoleAndPlayer
import java.util.concurrent.CopyOnWriteArrayList

interface ZeroRoundView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showList(list: CopyOnWriteArrayList<RoleAndPlayer>)

    @StateStrategyType(SkipStrategy::class)
    fun openBindRoleDialog(role: Role, playerList: ArrayList<String>)

    @StateStrategyType(SkipStrategy::class)
    fun openUnBindRoleDialog(pos: Int)

    @StateStrategyType(SkipStrategy::class)
    fun notifyPlayerListChanged()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showFab()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hideFab()
}