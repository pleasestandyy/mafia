package com.example.mafia.ui.mainMenu

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface MainMenuView: MvpView {
    fun openNewGameFragment(gameId: Long, gameName: String)
}