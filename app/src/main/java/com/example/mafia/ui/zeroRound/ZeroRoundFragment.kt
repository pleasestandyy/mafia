package com.example.mafia.ui.zeroRound

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.mafia.Const
import com.example.mafia.R
import com.example.mafia.data.model.Role
import com.example.mafia.data.model.RoleAndPlayer
import com.example.mafia.listAdapter.ListAdapter
import com.example.mafia.listAdapter.holder.RoleAndPlayerHolder
import com.example.mafia.listAdapter.interfaces.OnItemClickListner
import com.example.mafia.ui.interfaces.InizializbleViews
import kotlinx.android.synthetic.main.zero_round_fragment.*
import java.util.concurrent.CopyOnWriteArrayList

class ZeroRoundFragment : MvpAppCompatFragment(), ZeroRoundView, InizializbleViews{

    companion object{
        fun getInstance(id: Long): ZeroRoundFragment{
            val fragment = ZeroRoundFragment()
            val bundle = Bundle()

            bundle.putLong(Const.GAME_ID, id)
            fragment.arguments = bundle

            return fragment
        }
    }
    @InjectPresenter
    lateinit var presenter: ZeroRoundPresenter

    @ProvidePresenter
    fun providePresenter(): ZeroRoundPresenter{
        return ZeroRoundPresenter(arguments!!.getLong(Const.GAME_ID))
    }

    private lateinit var playerAndRoleListAdapter: ListAdapter<RoleAndPlayer>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View? = inflater.inflate(R.layout.zero_round_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        inizializeViews()
    }
    override fun inizializeViews() {
        playerAndRoleListAdapter = ListAdapter(R.layout.player_role_item, RoleAndPlayerHolder::class)
        playerAndRoleListAdapter.onItemClickListner = object : OnItemClickListner {
            override fun onitemClick(position: Int) {
                presenter.openRoleItem(position)
            }
        }
        recyclerViewZeroRound.layoutManager = LinearLayoutManager(context)
        recyclerViewZeroRound.adapter = playerAndRoleListAdapter
        fab.hide()

    }

    override fun showList(list: CopyOnWriteArrayList<RoleAndPlayer>) {
        playerAndRoleListAdapter.setList(list)
        playerAndRoleListAdapter.notifyDataSetChanged()
    }

    override fun openBindRoleDialog(role: Role, playerList: ArrayList<String>) {

        MaterialDialog(activity!!).show {
            title(R.string.choose_player)
            listItems(items = playerList) { dialog, index, text ->
                presenter.bindRoleToPlayer(index, role)
            }
        }
    }

    override fun openUnBindRoleDialog(pos: Int) {
        MaterialDialog(activity!!).show {
            title(R.string.cansel_bind_role_and_player)
            positiveButton(R.string.yes) {
                presenter.unbind(pos)
            }
            negativeButton(R.string.no)
        }
    }

    override fun notifyPlayerListChanged() {
        playerAndRoleListAdapter.notifyDataSetChanged()

    }

    override fun showFab() {
        fab.show()
    }

    override fun hideFab() {
        fab.hide()
    }
}
