package com.example.mafia.di.module

import android.app.Application
import androidx.room.Room
import com.example.mafia.data.MafiaDatabase
import dagger.Module
import dagger.Provides

@Module
class DBModule(var app : Application){

    var db : MafiaDatabase = Room.databaseBuilder(app, MafiaDatabase::class.java, "database").build()

    @Provides
    fun providesRoomDB():MafiaDatabase = db
}