package com.example.mafia.di

import com.example.mafia.MainActivity
import com.example.mafia.di.module.AppModule
import com.example.mafia.di.module.DBModule
import com.example.mafia.ui.mainMenu.MainMenuPresenter
import com.example.mafia.ui.continueGame.ContinueGamePresenter
import com.example.mafia.ui.zeroRound.ZeroRoundPresenter
import com.example.mafia.ui.newGame.NewGameFragment
import com.example.mafia.ui.newGame.NewGamePresenter
import dagger.Component

@Component(modules = [DBModule::class, AppModule::class])
interface AppComponent {

    fun inject(activity: MainActivity)

    fun inject(fragment: NewGameFragment)

    fun inject(presenter: NewGamePresenter)

    fun inject(presenter: ContinueGamePresenter)

    fun inject(presenter: MainMenuPresenter)

    fun inject(presenter: ZeroRoundPresenter)
}