package com.example.mafia.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.mafia.data.dao.GameDao
import com.example.mafia.data.dao.PlayerDao
import com.example.mafia.data.dao.RoleDao
import com.example.mafia.data.model.Game
import com.example.mafia.data.model.Player
import com.example.mafia.data.model.Role

@Database(entities = [Player::class, Role::class, Game::class], version = 1,exportSchema = false)
abstract class MafiaDatabase : RoomDatabase() {
    abstract fun roleDao() : RoleDao
    abstract fun playerDao() : PlayerDao
    abstract fun gameDao() : GameDao

}