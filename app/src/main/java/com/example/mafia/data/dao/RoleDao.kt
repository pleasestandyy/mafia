package com.example.mafia.data.dao

import androidx.room.*
import com.example.mafia.data.model.Role


@Dao
interface RoleDao {

    @Query("SELECT * FROM role")
    suspend fun getAll(): List<Role>

    @Query("SELECT * FROM role WHERE gameId = :gameId")
    suspend fun getAllByIdGame(gameId: Long): List<Role>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(role: Role): Long

    @Update
    suspend fun update(role: Role)

    @Delete
    suspend fun delete(role: Role)
}