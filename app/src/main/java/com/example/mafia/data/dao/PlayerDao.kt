package com.example.mafia.data.dao

import androidx.room.*
import com.example.mafia.data.model.Player

@Dao
interface PlayerDao {

    @Query("SELECT * FROM player")
    suspend fun getAll(): List<Player>

    @Query("SELECT * FROM player WHERE gameId = :gameId")
    suspend fun getAllByIdGame(gameId: Long): List<Player>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(player: Player) : Long

    @Update
    suspend fun update(player: Player)

    @Delete
    suspend fun delete(player: Player)
}