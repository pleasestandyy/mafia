package com.example.mafia.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Game(
    @PrimaryKey(autoGenerate = true)
    var id : Long? = null,
    var name: String?,
    var gamePhase: Int? = 0,
    var date: String? = ""
) {
}