package com.example.mafia.data.model

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import com.example.mafia.Const

@Entity(foreignKeys = [
    ForeignKey(
        entity = Game::class,
        parentColumns = ["id"],
        childColumns = ["gameId"],
        onDelete = CASCADE
    )]
)
class Role(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var priority: Int?,
    var title: String?,
    var isWakeUpToNight: Boolean?
): Comparable<Role> {
    var descripotion: String? = ""
    var color: Int? = Const.GRAY_COLOR_DEFAULT
    var gameId: Long? = null

    override fun compareTo(other: Role): Int
         = priority!!.compareTo(other.priority!!)
}