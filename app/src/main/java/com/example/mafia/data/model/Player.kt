package com.example.mafia.data.model

import androidx.room.*
import androidx.room.ForeignKey.CASCADE

@Entity(foreignKeys = [
    ForeignKey(
        entity = Game::class,
        parentColumns = ["id"],
        childColumns = ["gameId"],
        onDelete = CASCADE
    ),
    ForeignKey(
        entity = Role::class,
        parentColumns = ["id"],
        childColumns = ["roleId"]
    )
])
class Player(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var priority: Int?,
    var name: String?
) : Comparable<Player>{
    @ColumnInfo(index = true)
    var gameId: Long? = null
    @ColumnInfo(index = true)
    var roleId: Long? = null

    override fun compareTo(other: Player): Int
            = priority!!.compareTo(other.priority!!)
}