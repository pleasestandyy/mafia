package com.example.mafia.data.dao

import androidx.room.*
import com.example.mafia.data.model.Game

@Dao
interface GameDao {
    @Query("SELECT * FROM game")
    suspend fun getAll(): List<Game>

    @Query("SELECT name FROM game WHERE id = :id")
    suspend fun getGameName(id: Long): String

    @Query("SELECT * FROM game WHERE id = :id")
    suspend fun getGameById(id: Long): Game

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(game: Game) : Long

    @Update
    suspend fun update(game: Game)

    @Delete
    suspend fun delete(game: Game)
}