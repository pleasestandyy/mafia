package com.example.mafia

import android.app.Application
import com.example.mafia.di.AppComponent
import com.example.mafia.di.DaggerAppComponent
import com.example.mafia.di.module.AppModule
import com.example.mafia.di.module.DBModule


class MafiaApp : Application() {

    companion object {
        private lateinit var appComponent: AppComponent
        fun getAppComponent() : AppComponent = appComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .dBModule(DBModule(this))
            .appModule(AppModule(this))
            .build()
    }
}