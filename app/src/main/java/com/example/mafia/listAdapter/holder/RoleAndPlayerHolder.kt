package com.example.mafia.listAdapter.holder

import android.view.View
import android.widget.TextView
import com.example.mafia.R
import com.example.mafia.data.model.RoleAndPlayer
import com.example.mafia.listAdapter.AbsHolder

class RoleAndPlayerHolder(itemView: View) : AbsHolder(itemView){
    private val textViewPlayerName = itemView.findViewById<TextView>(R.id.textViewPlayerName)

    private val textViewRoleName= itemView.findViewById<TextView>(R.id.textViewRoleName)

    override fun bind(item: Any?) {
        item as RoleAndPlayer
        textViewRoleName.text = item.role!!.title
        textViewPlayerName.text = item.player?.name ?: "Нет игрока"
        textViewRoleName.setTextColor(item.role?.color!!)
        itemView.setOnClickListener {
            onItemClickListner!!.onitemClick(adapterPosition)
        }
    }
}