package com.example.mafia.listAdapter.holder

import android.view.View
import android.widget.TextView
import com.example.mafia.R
import com.example.mafia.data.model.Game
import com.example.mafia.listAdapter.AbsHolder

class GameHolder(itemView: View) : AbsHolder(itemView) {

    private val textViewGameName = itemView.findViewById<TextView>(R.id.textViewGameName)

    private val textViewGameDate = itemView.findViewById<TextView>(R.id.textViewGameDate)

    override fun bind(item: Any?) {
        item as Game
        textViewGameName.text = item.name
        textViewGameDate.text = item.date
        itemView.setOnClickListener {
            onItemClickListner!!.onitemClick(adapterPosition)
        }
    }

}