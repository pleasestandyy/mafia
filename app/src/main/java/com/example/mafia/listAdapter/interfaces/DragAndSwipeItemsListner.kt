package com.example.mafia.listAdapter.interfaces

interface DragAndSwipeItemsListner {

    fun onSwapItems(pos1 : Int, pos2 : Int)

    fun onSwipeItem(pos : Int)
}