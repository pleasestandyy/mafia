package com.example.mafia.listAdapter.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.mafia.R
import com.example.mafia.data.model.Role
import com.example.mafia.listAdapter.AbsHolder

class RoleHolder(itemView: View) : AbsHolder(itemView) {
    private val textViewTitle = itemView.findViewById<TextView>(R.id.title)

    private val imageViewIsWakeUp = itemView.findViewById<ImageView>(R.id.imageViewWakeUp)
    override fun bind(item: Any?) {
        item as Role
        textViewTitle.text = item.title
        textViewTitle.setTextColor(item.color!!)
        if(item.isWakeUpToNight!!)
            imageViewIsWakeUp.setImageResource(R.drawable.ic_wake_up)
        else
            imageViewIsWakeUp.setImageResource(R.drawable.ic_is_not_wake_up)
    }

}
