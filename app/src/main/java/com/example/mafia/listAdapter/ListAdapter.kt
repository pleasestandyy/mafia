package com.example.mafia.listAdapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mafia.listAdapter.interfaces.ItemTouchHelperAdapter
import com.example.mafia.listAdapter.interfaces.DragAndSwipeItemsListner
import com.example.mafia.listAdapter.interfaces.OnItemClickListner
import kotlin.reflect.KClass
import java.util.Collections.swap
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.reflect.full.primaryConstructor


class ListAdapter<T>(var resId: Int, var holderType: KClass<out AbsHolder>) : RecyclerView.Adapter<AbsHolder>(),
    ItemTouchHelperAdapter {

    var items: CopyOnWriteArrayList<T> = CopyOnWriteArrayList()

    lateinit var holder: AbsHolder

    var onDragAndSwipeItemsListner: DragAndSwipeItemsListner? = null

    var onItemClickListner : OnItemClickListner? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbsHolder {
        val view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
        holder = holderType.primaryConstructor!!.call(view)
        return holder
    }
    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: AbsHolder, position: Int) {
        if(onItemClickListner != null)
            holder.onItemClickListner = onItemClickListner
        holder.bind(items[position])

    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                swap(items, i, i + 1)
                if(onDragAndSwipeItemsListner != null)
                    onDragAndSwipeItemsListner!!.onSwapItems(i, i+1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1 ) {
                swap(items, i, i - 1)
                if(onDragAndSwipeItemsListner != null)
                    onDragAndSwipeItemsListner!!.onSwapItems(i, i-1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemDismiss(position: Int) {
        if(onDragAndSwipeItemsListner != null)
            onDragAndSwipeItemsListner!!.onSwipeItem(position)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): T{
        return items.get(position)
    }
    fun getitems():List<T>{
        return items
    }
    fun setList(items: CopyOnWriteArrayList<T>) {
        this.items = items
    }

    fun addItem(item: T){
        items.add(item)
    }
    fun size() : Int = items.size

}