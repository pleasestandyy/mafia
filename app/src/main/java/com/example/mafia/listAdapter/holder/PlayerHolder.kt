package com.example.mafia.listAdapter.holder

import android.view.View
import android.widget.TextView
import com.example.mafia.R
import com.example.mafia.data.model.Player
import com.example.mafia.listAdapter.AbsHolder

class PlayerHolder(itemView: View) : AbsHolder(itemView) {
    private val textViewName = itemView.findViewById<TextView>(R.id.textViewNamePlayer)

    override fun bind(item: Any?) {
        item as Player
        textViewName.setText(item.name)
    }
}